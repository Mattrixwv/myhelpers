//myHelpers/Algorithms.h
//Matthew Ellison
// Created: 03-10-19
//Modified: 03-10-19
//This file contains the declarations and implementations to several algorithms that I have found useful
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ALGORITHMS_H
#define ALGORITHMS_H


#include <inttypes.h>
#include <math.h>
#include <stdbool.h>
#include "DynamicInt64Array.h"

//This is a function that performs a bubble sort on an array of int64_t
void bubbleSortInt64(int64_t* nums, uint64_t size){
	//Keep track of elements that have been sorted
	for(uint64_t sorted = 0;sorted < size;++sorted){
		//Look at every element in the ary, moving the largest element to the end
		for(uint64_t location = 1;location < (size - sorted);++location){
			//If the current element is smaller than the last swap them
			if(nums[location] < nums[location - 1]){
				int64_t temp = nums[location];
				nums[location] = nums[location - 1];
				nums[location - 1] = temp;
			}
		}
	}
}

//This is a helper function of quickSortInt64. It chooses a pivot element and sort everything to larger and smaller sides
uint64_t partitionInt64(int64_t* ary, uint64_t bottom, uint64_t top){
	int64_t pivot = ary[top];	//Choose a pivot element
	int64_t smaller = bottom - 1;	//Keep track of the location of all elements smaller than pivot

	//Loop through the array, looking for elements that are smaller than pivot and move them to the front of the array
	for(uint64_t location = bottom;location < top;++location){
		//If the current element is smaller than the pivot move it to the front of the array and move the tracker
		if(ary[location] < pivot){
			++smaller;	//Increment the smaller than location tracker

			//Swap the element to the correct location
			int64_t temp = ary[location];
			ary[location] = ary[smaller];
			ary[smaller] = temp;
		}
	}

	//Move the pivot element to the corrent location
	++smaller;
	int64_t temp = ary[smaller];
	ary[smaller] = ary[top];
	ary[top] = temp;

	//Return the location of the pivot element
	return smaller;
}

//This is a function that performs a quick sort on an array of int64_t
void quickSortInt64(int64_t* nums, uint64_t bottom, uint64_t top){
	//Make sure you are working on a valid slice of the array
	if(bottom < top){
		//Get the pivot element
		uint64_t pivot = partitionInt64(nums, bottom, top);

		//Sort all elements smaller than the pivot
		quickSortInt64(nums, bottom, pivot - 1);
		//Sort all elements larger than the pivot
		quickSortInt64(nums, pivot + 1, top);
	}
}

//This is a function that returns all the primes <= goalNumber and returns a vector with those prime numbers
struct DynamicInt64Array getPrimes(int64_t goalNumber){
	struct DynamicInt64Array primes;
	initDynamicInt64Array(&primes);
	bool foundFactor = false;

	//If the number is 1, 0, or a negative number return an empty vector
	if(goalNumber <= 1){
		return primes;
	}
	else{
		pushBackDynamicInt64Array(&primes, 2);
	}
	//We can now start at 3 and skip all of the even numbers
	for(int64_t possiblePrime = 3;possiblePrime <= goalNumber;possiblePrime += 2){
		//Step through every element in the current primes. If you don't find anything that divides it, it must be a prime itself
		uint64_t topPossibleFactor = ceil(sqrt(possiblePrime));
		for(uint64_t cnt = 0;(cnt < primes.size) && (primes.ptr[cnt] <= topPossibleFactor);++cnt){
			if((possiblePrime % primes.ptr[cnt]) == 0){
				foundFactor = true;
				break;
			}
		}
		//If you didn't find a factor then it must be prime
		if(!foundFactor){
			pushBackDynamicInt64Array(&primes, possiblePrime);
		}
		//If you did find a factor you need to reset the flag
		else{
			foundFactor = false;
		}
	}

	bubbleSortDynamicInt64Array(&primes);
	return primes;
}

//This function returns a DynamicInt64Array with a specific number of primes
struct DynamicInt64Array getNumPrimes(int64_t numberOfPrimes){
	struct DynamicInt64Array primes;
	initDynamicInt64Array(&primes);
	reserveDynamicInt64Array(&primes, numberOfPrimes);	//Saves cycles later
	bool foundFactor = false;

	//If the number is 1, 0, or a negative number return an empty vector
	if(numberOfPrimes <= 1){
		return primes;
	}
	//Otherwise 2 is the first prime number
	else{
		pushBackDynamicInt64Array(&primes, 2);
	}

	//Loop through every odd number starting at 3 until we find the requisite number of primes
	//Using possiblePrime >= 3 to make sure it doesn't loop back around in an overflow error and create an infinite loop
	for(int64_t possiblePrime = 3;(primes.size < numberOfPrimes) && (possiblePrime >= 3);possiblePrime += 2){
		//Step through every element in the current primes. If you don't find anything that divides it, it must be a prime itself
		uint64_t topPossibleFactor = ceil(sqrt(possiblePrime));
		for(uint64_t cnt = 0;(cnt < primes.size) && (getDynamicInt64Array(&primes, cnt) <= topPossibleFactor);++cnt){
			if((possiblePrime % getDynamicInt64Array(&primes, cnt)) == 0){
				foundFactor = true;
				break;
			}
		}
		//If you didn't find a factor then it must be prime
		if(!foundFactor){
			pushBackDynamicInt64Array(&primes, possiblePrime);
		}
		//If you did find a factor you need to reset the flag
		else{
			foundFactor = false;
		}
	}

	//The numbers should be in order, but sort them anyway just in case
	bubbleSortDynamicInt64Array(&primes);
	return primes;
}

//This function returns all primes factors of a number
struct DynamicInt64Array getFactors(int64_t goalNumber){
	//Get all the prime numbers up to sqrt(number). If there is a prime < goalNumber it will have to be <= sqrt(goalNumber)
	struct DynamicInt64Array primes = getPrimes((int64_t)ceil(sqrt(goalNumber)));	//Make sure you are getting a vector of the correct type
	struct DynamicInt64Array factors;
	initDynamicInt64Array(&factors);

	//Need to step through each prime and see if it is a factor of the number
	for(int64_t cnt = 0;cnt < primes.size;){
		if((goalNumber % getDynamicInt64Array(&primes, cnt)) == 0){
			pushBackDynamicInt64Array(&factors, getDynamicInt64Array(&primes, cnt));
			goalNumber /= getDynamicInt64Array(&primes, cnt);
		}
		else{
			++cnt;
		}
	}
	//If it didn't find any factors in the primes the number itself must be prime
	if(factors.size == 0){
		pushBackDynamicInt64Array(&factors, goalNumber);
		goalNumber /= goalNumber;
	}

	///Should add some kind of error throwing inc ase the number != 1 after searching for all prime factors

	return factors;
}

//This is a function that gets all the divisors of num and returns a DynamicInt64Array containing the divisors
struct DynamicInt64Array getDivisors(int64_t num){
	struct DynamicInt64Array divisors;	//Holds the number of divisors
	initDynamicInt64Array(&divisors);

	//Ensure the parameter is a valid number
	if(num <= 0){
		return divisors;
	}
	else if(num == 1){
		pushBackDynamicInt64Array(&divisors, 1);
		return divisors;
	}
	//You only need to check up to sqrt(num)
	int64_t topPossibleDivisor = ceil(sqrt(num));
	for(int64_t possibleDivisor = 1;possibleDivisor <= topPossibleDivisor;++possibleDivisor){
		//Check if the counter evenly divides the number
		//If it does the counter and the other number are both divisors
		if((num % possibleDivisor) == 0){
			//We don't need to check if the number already exists because we are only checking numbers <= sqrt(num), so there can be no duplicates
			pushBackDynamicInt64Array(&divisors, possibleDivisor);
			//We still need to account for sqrt(num) being a divisor
			if(possibleDivisor != topPossibleDivisor){
				pushBackDynamicInt64Array(&divisors, (num / possibleDivisor));
			}
			//Take care of a few occations where a number was added twice
			if(getDynamicInt64Array(&divisors, (divisors.size - 1)) == (possibleDivisor + 1)){
				++possibleDivisor;
			}
		}
	}
	//Sort the vector for neatness
	bubbleSortDynamicInt64Array(&divisors);

	//Return the vector of divisors
	return divisors;
}

//This function returns the numth Fibonacci number
int64_t getFib(const int64_t num){
	//Make sure the number is within bounds
	if(num <= 2){
		return 1;
	}
	//Setup the variables
	int64_t fib = 0;
	int64_t tempNums[3];
	tempNums[0] = tempNums[1] = 1;

	//Do the calculation
	unsigned int cnt;
	for(cnt = 2;(cnt < num) && (tempNums[(cnt - 1) % 3] >= tempNums[(cnt - 2) % 3]);++cnt){
		tempNums[cnt % 3] = tempNums[(cnt + 1) % 3] + tempNums[(cnt + 2) % 3];
	}
	fib = tempNums[(cnt - 1) % 3];	//Transfer the answer to permanent variable. -1 to account for the offset of starting at 0

	return fib;
}

//This function returns a DynamicInt64Array that includes all Fibonacci numbers <= num
struct DynamicInt64Array getAllFib(const int64_t num){
	struct DynamicInt64Array fibList;
	initDynamicInt64Array(&fibList);

	//Make sure the number is within bounds
	if(num <= 1){
		pushBackDynamicInt64Array(&fibList, 1);
		return fibList;
	}
	else{	//Make sure to add the first 2 elements
		pushBackDynamicInt64Array(&fibList, 1);
		pushBackDynamicInt64Array(&fibList, 1);
	}

	//Setup the variables
	int64_t fib = 0;
	int64_t tempNums[3];
	tempNums[0] = tempNums[1] = 1;

	//Do the calculation and add each number to the vector
	for(int64_t cnt = 2;(tempNums[(cnt - 1) % 3] < num) && (tempNums[(cnt - 1) % 3] >= tempNums[(cnt - 2) % 3]);++cnt){
		tempNums[cnt % 3] = tempNums[(cnt + 1) % 3] + tempNums[(cnt + 2) % 3];
		pushBackDynamicInt64Array(&fibList, tempNums[cnt % 3]);
	}

	//If you triggered the exit statement you have one more element than you need
	popBackDynamicInt64Array(&fibList);

	//Return the vector that contains all of the Fibonacci numbers
	return fibList;
}

#endif //ALGORITHMS_H
