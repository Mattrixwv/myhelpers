//Programs/C/myHelpers/Stopwatch.h
//Matthew Ellison
// Created: 03-08-19
//Modified: 03-08-19
//This struct helps by using a struct to simplify timing a program in C
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef STOPWATCH_H
#define STOPWATCH_H


#include <time.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>


struct Stopwatch{
	uint64_t startTime;
	uint64_t stopTime;
};

void initStopwatch(struct Stopwatch* timer){
	//You need everything to start at 0
	timer->startTime = timer->stopTime = (uint64_t)0;
}

void resetStopwatch(struct Stopwatch* timer){
	//Reseting is basically the same as initializing
	initStopwatch(timer);
}

void startStopwatch(struct Stopwatch* timer){
	//If you just started the stopwatch make sure the stop time is 0
	timer->stopTime = (uint64_t)0;
	//Record the start time
	timer->startTime = clock();
}

void stopStopwatch(struct Stopwatch* timer){
	//Get the stop time as close to calling as possible
	timer->stopTime = clock();
	//If the stopwatch was never started pretend like you never recorded anything in stop
	if(timer->startTime == (uint64_t)0){
		timer->stopTime = (uint64_t)0;
	}
}

uint64_t getTimeStopwatch(struct Stopwatch* timer){
	uint64_t tempTime = clock();
	//If everything is performing normally return what is expected
	if((timer->stopTime != (uint64_t)0) && (timer->startTime != (uint64_t)0)){
		return (timer->stopTime - timer->startTime);
	}
	//If the stopwatch has been started but not stopped return the current time as the stop time. Simulates looking at it while it's still running
	else if((timer->startTime != (uint64_t)0) && (timer->stopTime == (uint64_t)0)){
		return (tempTime - timer->startTime);
	}
	//If the stopwatch was never started return 0
	else{
		return (uint64_t)0;
	}
}

//Gets the time from a stopwatch at nanosecond resolution
uint64_t getNanoStopwatch(struct Stopwatch* timer){
	return ((getTimeStopwatch(timer) / (double)CLOCKS_PER_SEC) * 1000000000);
}

//Gets the time from a stopwatch at microsecond resolution
double getMicroStopwatch(struct Stopwatch* timer){
	return ((getTimeStopwatch(timer) / (double)CLOCKS_PER_SEC) * 1000000);
}

//Gets the time from a stopwatch at millisecond resolution
double getMilliStopwatch(struct Stopwatch* timer){
	return ((getTimeStopwatch(timer) / (double)CLOCKS_PER_SEC) * 1000);
}

//Gets the time from a stopwatch at second resolution
double getSecondStopwatch(struct Stopwatch* timer){
	return (getTimeStopwatch(timer) / (double)CLOCKS_PER_SEC);
}

//Gets the time from a stopwatch at minute resolution
double getMinuteStopwatch(struct Stopwatch* timer){
	return ((getTimeStopwatch(timer) / (double)CLOCKS_PER_SEC) / 60);
}

//Gets the time from a stopwatch at hour resolution
double getHourStopwatch(struct Stopwatch* timer){
	return ((getTimeStopwatch(timer) / (double)CLOCKS_PER_SEC) / 360);
}

//Gets the time from a stopwatch at the "Best" resolution. The goal is XXX.XXX [Resolution]
//While it would be possible to use this function to get the time while still running it is not recomended because of the time it takes to return the string
char* getStrStopwatch(struct Stopwatch* timer){
	char* num = (char*)malloc(35 * sizeof(char));	//Holds the string that will be created in the end
	double dur;	//Holds the duration the stopwatch has run

	//Decide what the best way to get time is. Looking for a XXX.XXX format
	if(getNanoStopwatch(timer) < 1000){
		dur = getNanoStopwatch(timer);
		int intDur = dur;	//Holds the whole number integer representation of the duration
		int fractDur = ((dur - intDur) * 1000);	//Holds the integer representation of the fractional part of the duration
		snprintf(num, 35, "%d.%03d nanoseconds", intDur, fractDur);
	}
	else if(getMicroStopwatch(timer) < 1000){
		dur = getMicroStopwatch(timer);
		int intDur = dur;	//Holds the whole number integer representation of the duration
		int fractDur = ((dur - intDur) * 1000);	//Holds the integer representation of the fractional part of the duration
		snprintf(num, 35, "%d.%03d microseconds", intDur, fractDur);
	}
	else if(getMilliStopwatch(timer) < 1000){
		dur = getMilliStopwatch(timer);
		int intDur = dur;	//Holds the whole number integer representation of the duration
		int fractDur = ((dur - intDur) * 1000);	//Holds the integer representation of the fractional part of the duration
		snprintf(num, 35, "%d.%03d milliseconds", intDur, fractDur);
	}
	else if(getSecondStopwatch(timer) < 1000){
		dur = getSecondStopwatch(timer);
		int intDur = dur;	//Holds the whole number integer representation of the duration
		int fractDur = ((dur - intDur) * 1000);	//Holds the integer representation of the fractional part of the duration
		snprintf(num, 35, "%d.%03d seconds", intDur, fractDur);
	}
	else if(getMinuteStopwatch(timer) < 1000){
		dur = getMinuteStopwatch(timer);
		int intDur = dur;	//Holds the whole number integer representation of the duration
		int fractDur = ((dur - intDur) * 1000);	//Holds the integer representation of the fractional part of the duration
		snprintf(num, 35, "%d.%03d minutes", intDur, fractDur);
	}
	else{
		dur = getHourStopwatch(timer);
		int intDur = dur;	//Holds the whole number integer representation of the duration
		int fractDur = ((dur - intDur) * 1000);	//Holds the integer representation of the fractional part of the duration
		snprintf(num, 21, "%d.%03d hours", intDur, fractDur);
	}

	return num;
}


#endif //STOPWATCH_H
