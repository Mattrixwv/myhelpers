//myHelper/DynamicInt64Array.h
//Matthew Ellison
// Created: 03-08-19
//Modified: 03-10-19
//This is the test for my dynamic array in c
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include "DynamicInt64Array.h"


int main(){
	printf("BEGIN TESTS\n\n\n");

	//Define a dynamic array
	struct DynamicInt64Array testAry;

	//Initialize the array
	initDynamicInt64Array(&testAry);

	//Set some elements in the array
	pushBackDynamicInt64Array(&testAry, 0);
	pushBackDynamicInt64Array(&testAry, 1);
	pushBackDynamicInt64Array(&testAry, 2);
	pushBackDynamicInt64Array(&testAry, 3);
	pushBackDynamicInt64Array(&testAry, 4);
	pushBackDynamicInt64Array(&testAry, 5);

	if((testAry.ptr[0] == 0) && (testAry.ptr[1] == 1) && (testAry.ptr[2] == 2) && (testAry.ptr[3] == 3) && (testAry.ptr[4] == 4) && (testAry.ptr[5] == 5)){
		printf("Adding numbers to the array worked correctly\n");
	}
	else{
		printf("Something went wrong adding numbers to the array\n");
		printf("The numbers are: %d %d %d %d %d %d\n", testAry.ptr[0], testAry.ptr[1], testAry.ptr[2], testAry.ptr[3], testAry.ptr[4], testAry.ptr[5]);
		return 1;
	}

	//Check that the size is correct
	if(testAry.size != 6){
		printf("Something is wrong with the size: %d\nIt should be &d\n", testAry.size, 6);
		return 1;
	}
	if(testAry.allocated != 8){
		printf("Something is wrong with the allocated size: %d\nIt should be %d\n", testAry.allocated, 8);
		return 1;
	}

	//Get some elements from the array
	if((getDynamicInt64Array(&testAry, 0) == 0) && (getDynamicInt64Array(&testAry, 4) == 4) &&  (getDynamicInt64Array(&testAry, 6) == 0)){
		printf("The get function is working correctly\n");
	}
	else{
		printf("The get function has a problem. The numbers are:\n%d %d %d\nThe numbers should be:\n%d %d %d", getDynamicInt64Array(&testAry, 0), getDynamicInt64Array(&testAry, 4), getDynamicInt64Array(&testAry, 6), 0, 4, 0);
		return 1;
	}

	//Search the array for an element
	if((findDynamicInt64Array(&testAry, 5) == 5) && (findDynamicInt64Array(&testAry, 3) == 3) && (findDynamicInt64Array(&testAry, 7) == -1)){
		printf("The find function is working correctly\n");
	}
	else{
		printf("The find function has a problem\nThe numbers are: %d %d %d\nThey should be: %d %d %d\n", findDynamicInt64Array(&testAry, 5), findDynamicInt64Array(&testAry, 3), findDynamicInt64Array(&testAry, 7), 5, 3, -1);
		return 1;
	}

	//Add an element to the array
	pushBackDynamicInt64Array(&testAry, 6);
	if(testAry.ptr[6] == 6){
		printf("The pushback function is working correctly\n");
	}
	else{
		printf("There is a problem in the pushback function\n");
		return 1;
	}
	if(testAry.size != 7){
		printf("There is a problem with the size after adding to the array\n");
		printf("The size is %d and it should be %d\n", testAry.size, 7);
		return 1;
	}

	//Remove an element from the array
	removeDynamicInt64Array(&testAry, 2);
	if((testAry.ptr[2] == 3) && (testAry.ptr[3] == 4) && (testAry.ptr[4] == 5) &&  (testAry.ptr[5] == 6)){
		printf("Removing funcion is working correctly\n");
	}
	else{
		printf("There is a problem with the remove function\n");
		return 1;
	}
	if(testAry.size != 6){
		printf("There is a problem with the size after using the remove function\n");
		return 1;
	}

	removeLocationDynamicInt64Array(&testAry, 4);
	if(testAry.ptr[4] == 6){
		printf("RemoveLocation function is working correctly\n");
	}
	else{
		printf("There is a problem with the removeLocation function\n");
		printf("The number is %d it should be %d\n", testAry.ptr[4], 6);
		return 1;
	}
	if(testAry.size != 5){
		printf("There is a problem with the size after using the removeLocation function\n");
		return 1;
	}

	//Test the is sorted function
	if(!isSortedDynamicInt64Array(&testAry)){
		printf("There is something wrong with the isSorted function: Test 1\n");
		return 1;
	}
	pushBackDynamicInt64Array(&testAry, 2);
	if(isSortedDynamicInt64Array(&testAry)){
		printf("There is something wrong with the isSorted function: Test2\n");
		return 1;
	}

	//Test the bubbleSort function
	bubbleSortDynamicInt64Array(&testAry);
	if(isSortedDynamicInt64Array(&testAry)){
		printf("BubbleSort is working correctly\n");
	}
	else{
		printf("There is something wrong with bubbleSort\n");
		return 1;
	}

	//Test the quickSort function
	pushBackDynamicInt64Array(&testAry, 5);
	quickSortDynamicInt64Array(&testAry);
	if(isSortedDynamicInt64Array(&testAry)){
		printf("QuickSort is working correctly\n");
	}
	else{
		printf("There is something wrong with quickSort\n");
		return 1;
	}

	//Test the getProd function
	if(getSumDynamicInt64Array(&testAry) == 21){
		printf("getSum is working correctly\n");
	}
	else{
		printf("There is something wrong with getSum\nThe sum returned is %d\nThe sum should be %d", getSumDynamicInt64Array(&testAry), 21);
		return 1;
	}

	//Test the getSum function
	if(getProdDynamicInt64Array(&testAry) != 0){
		printf("There is something wrong with getProd\nThe product returned is %d\nThe product should be%d", getProdDynamicInt64Array(&testAry), 0);
		return 1;
	}
	//Pop off the first 0 so there will be a number in the product
	removeDynamicInt64Array(&testAry, 0);
	if(getProdDynamicInt64Array(&testAry) == 720){
		printf("getProd is working correctly\n");
	}
	else{
		printf("There is something wrong with getProd\nThe product returned is %d\nThe product should be%d", getProdDynamicInt64Array(&testAry), 720);
		return 1;
	}

	//Test copy function
	struct DynamicInt64Array testAry2;
	initDynamicInt64Array(&testAry2);
	copyDynamicInt64Array(&testAry, &testAry2);
	if((testAry.ptr[0] == testAry2.ptr[0]) && (testAry.ptr[1] == testAry2.ptr[1]) && (testAry.ptr[2] == testAry2.ptr[2]) && (testAry.ptr[3] == testAry2.ptr[3]) && (testAry.ptr[4] == testAry2.ptr[4]) && (testAry.ptr[5] == testAry2.ptr[5])){
		printf("copy is working correctly\n");
	}
	else{
		printf("There is something wrong with copy\n");
		return 1;
	}

	//Test compare function
	//Test equals
	if(compareDynamicInt64Array(&testAry, &testAry2) != 0){
		printf("There is a problem with compare equals\n");
		return 1;
	}
	//Test lessThan
	pushBackDynamicInt64Array(&testAry2, 7);
	if(compareDynamicInt64Array(&testAry, &testAry2) != -1){
		printf("There is a problem with compare less than\n");
		return 1;
	}
	//Test greaterThan
	if(compareDynamicInt64Array(&testAry2, &testAry) != 1){
		printf("There is a problem with compare greater than\n");
		return 1;
	}
	else{
		printf("compare is working correctly\n");
	}

	//Release all of the memory allocated by the array
	destroyDynamicInt64Array(&testAry);
	destroyDynamicInt64Array(&testAry2);

	printf("\n\nEND OF TESTS\n");

	return 0;
}

/* Results:
BEGIN TESTS


Adding numbers to the array worked correctly
The get function is working correctly
The find function is working correctly
The pushback function is working correctly
Removing funcion is working correctly
RemoveLocation function is working correctly
BubbleSort is working correctly
QuickSort is working correctly


END OF TESTS
*/
