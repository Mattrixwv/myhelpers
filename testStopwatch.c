//Programs/C/myHelpers/testStopwatch.c
//Matthew Ellison
// Created: 03-08-19
//Modified: 03-08-19
//This program is a simple test for the stopwatch struct and accompanying functions
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <inttypes.h>
#include "Stopwatch.h"

const int NUM_OF_RUNS = 10000;


int main(){
	struct Stopwatch timer;	//Create the stopwatch
	initStopwatch(&timer);	//Initialize the stopwatch

	//Start the stopwatch counting
	startStopwatch(&timer);

	//Perform some calculation
	for(int cnt = 0;cnt < NUM_OF_RUNS;++cnt){
		printf("%d\n", cnt);
	}

	//Stop the stopwatch counting
	stopStopwatch(&timer);

	//Get a string about the time
	char* timerStr = getStrStopwatch(&timer);

	//Print the results
	printf("It took %lu nanoseconds to run this program\n", getNanoStopwatch(&timer));
	printf("It took %f microseconds to run this program\n", getMicroStopwatch(&timer));
	printf("It took %f milliseconds to run this program\n", getMilliStopwatch(&timer));
	printf("It took %f seconds to run this program\n", getSecondStopwatch(&timer));
	printf("It took %f minutes to run this program\n", getMinuteStopwatch(&timer));
	printf("It took %f hours to run this program\n", getHourStopwatch(&timer));
	printf("It took %s to run this program\n", timerStr);

	//Free the memory used in the string
	free(timerStr);
	timerStr = NULL;

	return 0;
}

/* Results:
It took 1938000000 nanoseconds to run this program
It took 1938000.000000 microseconds to run this program
It took 1938.000000 milliseconds to run this program
It took 1.938000 seconds to run this program
It took 0.032300 minutes to run this program
It took 0.005383 hours to run this program
It took 1.938 seconds to run this program
*/
