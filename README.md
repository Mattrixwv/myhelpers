#myHelpers
These are some structures and functions that I have created that are not part of the standard library, but that I have found helpful to keep around.

#Stopwatch
This is a struct and set of functions that can be used to time portions of your code.
Usage example:
	struct Stopwatch timer;
	init(&timer);
	start(&timer);
	//Algorithm that you wish to time here
	stop(&timer);
	printf("It took %s to run this algorithm\n", getStr(&timer));
You can get automatic resolutions of time with the getStr() function, but you can also get specific resolutions of time with the other get functions.
These are the other get functions:
getNano(), getMicro(), getMilli(), getSecond(), getMinute(), getHour()
getNano() returns a uint64_t, but the rest of the get functions return doubles