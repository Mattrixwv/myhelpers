//myHelpers/Algorithms.h
//Matthew Ellison
// Created: 03-10-19
//Modified: 03-10-19
//This file contains some tests for the functions in the algorithms file I created
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "DynamicInt64Array.h"
#include "Algorithms.h"

bool testGetPrimes();
bool testGetNumPrimes();
bool testGetFactors();
bool tetsGetDivisors();
bool testGetFib();
bool testGetAllFib();
bool testBubbleSort();
bool testQuickSort();


int main(){
	printf("BEGIN TESTS\n\n\n");

	//Test the getPrimes function
	if(testGetPrimes()){
		printf("getPrimes worked correctly\n");
	}
	else{
		printf("getPrimes encountered an error\n");
		return 1;
	}

	//Test the getNumPrimes function
	if(testGetNumPrimes()){
		printf("getNumPrimes worked correctly\n");
	}
	else{
		printf("getNumPrimes encountered an error\n");
		return 1;
	}

	//Test the getFactors function
	if(testGetFactors()){
		printf("getFactors worked correctly\n");
	}
	else{
		printf("getFactors encountered an error\n");
		return 1;
	}

	//Test the getDivisors function
	if(tetsGetDivisors()){
		printf("getDivisors worked correctly\n");
	}
	else{
		printf("getDivisors encountered an error\n");
		return 1;
	}

	//Test the getFib function
	if(testGetFib()){
		printf("getFib worked correctly\n");
	}
	else{
		printf("getFib encountered an error\n");
		return 1;
	}

	//Test the getAllFib function
	if(testGetAllFib()){
		printf("getAllFib worked correctly\n");
	}
	else{
		printf("getAllFib encountered an error\n");
		return 1;
	}

	//Test the bubbleSort function
	if(testBubbleSort()){
		printf("bubbleSort worked correctly\n");
	}
	else{
		printf("bubbleSort encountered an error\n");
		return 1;
	}

	//Test the quickSort function
	if(testQuickSort()){
		printf("quickSort worked correctly\n");
	}
	else{
		printf("quickSort encountered an error\n");
		return 1;
	}

	printf("\n\n\nALL TESTS COMPLETE\n");

	return 0;
}

bool testGetPrimes(){
	struct DynamicInt64Array correctAnswer;
	initDynamicInt64Array(&correctAnswer);	//You need to initialize the structure
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 3);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	pushBackDynamicInt64Array(&correctAnswer, 7);
	pushBackDynamicInt64Array(&correctAnswer, 11);
	pushBackDynamicInt64Array(&correctAnswer, 13);
	pushBackDynamicInt64Array(&correctAnswer, 17);
	pushBackDynamicInt64Array(&correctAnswer, 19);
	pushBackDynamicInt64Array(&correctAnswer, 23);
	pushBackDynamicInt64Array(&correctAnswer, 29);
	pushBackDynamicInt64Array(&correctAnswer, 31);
	pushBackDynamicInt64Array(&correctAnswer, 37);
	pushBackDynamicInt64Array(&correctAnswer, 41);
	pushBackDynamicInt64Array(&correctAnswer, 43);
	pushBackDynamicInt64Array(&correctAnswer, 47);
	pushBackDynamicInt64Array(&correctAnswer, 53);
	pushBackDynamicInt64Array(&correctAnswer, 59);
	pushBackDynamicInt64Array(&correctAnswer, 61);
	pushBackDynamicInt64Array(&correctAnswer, 67);
	pushBackDynamicInt64Array(&correctAnswer, 71);
	pushBackDynamicInt64Array(&correctAnswer, 73);
	pushBackDynamicInt64Array(&correctAnswer, 79);
	pushBackDynamicInt64Array(&correctAnswer, 83);
	pushBackDynamicInt64Array(&correctAnswer, 89);
	pushBackDynamicInt64Array(&correctAnswer, 97);
	int64_t topNum = 100;
	struct DynamicInt64Array answer = getPrimes(topNum);
	//If the two arrays are not equal there is a problem
	if(compareDynamicInt64Array(&correctAnswer, &answer) != 0){
		return false;
	}

	//Destroy the arrays propperly to free the memory
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetNumPrimes(){
	struct DynamicInt64Array correctAnswer;
	initDynamicInt64Array(&correctAnswer);	//You need to initialize the structure
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 3);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	pushBackDynamicInt64Array(&correctAnswer, 7);
	pushBackDynamicInt64Array(&correctAnswer, 11);
	pushBackDynamicInt64Array(&correctAnswer, 13);
	pushBackDynamicInt64Array(&correctAnswer, 17);
	pushBackDynamicInt64Array(&correctAnswer, 19);
	pushBackDynamicInt64Array(&correctAnswer, 23);
	pushBackDynamicInt64Array(&correctAnswer, 29);
	pushBackDynamicInt64Array(&correctAnswer, 31);
	pushBackDynamicInt64Array(&correctAnswer, 37);
	pushBackDynamicInt64Array(&correctAnswer, 41);
	pushBackDynamicInt64Array(&correctAnswer, 43);
	pushBackDynamicInt64Array(&correctAnswer, 47);
	pushBackDynamicInt64Array(&correctAnswer, 53);
	pushBackDynamicInt64Array(&correctAnswer, 59);
	pushBackDynamicInt64Array(&correctAnswer, 61);
	pushBackDynamicInt64Array(&correctAnswer, 67);
	pushBackDynamicInt64Array(&correctAnswer, 71);
	pushBackDynamicInt64Array(&correctAnswer, 73);
	pushBackDynamicInt64Array(&correctAnswer, 79);
	pushBackDynamicInt64Array(&correctAnswer, 83);
	pushBackDynamicInt64Array(&correctAnswer, 89);
	pushBackDynamicInt64Array(&correctAnswer, 97);
	uint64_t numPrimes = 25;
	struct DynamicInt64Array answer = getNumPrimes(numPrimes);
	if(compareDynamicInt64Array(&correctAnswer, &answer) != 0){
		return false;
	}

	//Destroy the arrays propperly to free the memory
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetFactors(){
	struct DynamicInt64Array correctAnswer;
	initDynamicInt64Array(&correctAnswer);	//You need to initialize the structure
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	int64_t number = 100;
	struct DynamicInt64Array answer = getFactors(number);
	if(compareDynamicInt64Array(&correctAnswer, &answer) != 0){
		return false;
	}
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);

	initDynamicInt64Array(&correctAnswer);
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 7);
	pushBackDynamicInt64Array(&correctAnswer, 7);
	number = 98;
	answer = getFactors(number);
	if(compareDynamicInt64Array(&correctAnswer, &answer) != 0){
		return false;
	}

	//Destroy the arrays propperly to free the memory
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);

	//If a false was not triggered it must have passed all tests
	return true;
}

bool tetsGetDivisors(){
	struct DynamicInt64Array correctAnswer;
	initDynamicInt64Array(&correctAnswer);
	pushBackDynamicInt64Array(&correctAnswer, 1);
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 4);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	pushBackDynamicInt64Array(&correctAnswer, 10);
	pushBackDynamicInt64Array(&correctAnswer, 20);
	pushBackDynamicInt64Array(&correctAnswer, 25);
	pushBackDynamicInt64Array(&correctAnswer, 50);
	pushBackDynamicInt64Array(&correctAnswer, 100);
	int64_t topNum = 100;
	struct DynamicInt64Array answer = getDivisors(topNum);
	if(compareDynamicInt64Array(&correctAnswer, &answer) != 0){
		return false;
	}
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetFib(){
	//Test the imbeded type getFib function
	int64_t correctAnswer = 144;
	int64_t number = 12;
	int64_t answer = getFib(number);
	if(correctAnswer != answer){
		printf("getFib() failed at test 1");
		return false;
	}

	number = 20;
	correctAnswer = 6765;
	answer = getFib(number);
	if(correctAnswer != answer){
		printf("getFib() failed at test 2");
		return false;
	}


/*
	///Need to implement mpz functions
	//Test the gmp integer function
	mpz_class mpzNumber = 12;
	mpz_class longCorrectAnswer = 144;
	mpz_class longAnswer = mee::getFib(mpzNumber);
	if(longCorrectAnswer != longAnswer){
		std::cout << "getFib() for mpz failed at test 3" << std::endl;
		return false;
	}

	mpzNumber = 4782;
	longCorrectAnswer = "1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816";
	longAnswer = mee::getFib(mpzNumber);
	if(longCorrectAnswer != longAnswer){
		std::cout << "getFib() for mpzfailed at test 4" << std::endl;
		return false;
	}
*/

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetAllFib(){
	struct DynamicInt64Array correctAnswer;
	initDynamicInt64Array(&correctAnswer);
	pushBackDynamicInt64Array(&correctAnswer, 1);
	pushBackDynamicInt64Array(&correctAnswer, 1);
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 3);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	pushBackDynamicInt64Array(&correctAnswer, 8);
	pushBackDynamicInt64Array(&correctAnswer, 13);
	pushBackDynamicInt64Array(&correctAnswer, 21);
	pushBackDynamicInt64Array(&correctAnswer, 34);
	pushBackDynamicInt64Array(&correctAnswer, 55);
	pushBackDynamicInt64Array(&correctAnswer, 89);
	int64_t highestNumber = 100;
	struct DynamicInt64Array answer = getAllFib(highestNumber);
	if(compareDynamicInt64Array(&correctAnswer, &answer)){
		return false;
	}
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);
	initDynamicInt64Array(&correctAnswer);

	//Setup the correct answer
	pushBackDynamicInt64Array(&correctAnswer, 1);
	pushBackDynamicInt64Array(&correctAnswer, 1);
	pushBackDynamicInt64Array(&correctAnswer, 2);
	pushBackDynamicInt64Array(&correctAnswer, 3);
	pushBackDynamicInt64Array(&correctAnswer, 5);
	pushBackDynamicInt64Array(&correctAnswer, 8);
	pushBackDynamicInt64Array(&correctAnswer, 13);
	pushBackDynamicInt64Array(&correctAnswer, 21);
	pushBackDynamicInt64Array(&correctAnswer, 34);
	pushBackDynamicInt64Array(&correctAnswer, 55);
	pushBackDynamicInt64Array(&correctAnswer, 89);
	pushBackDynamicInt64Array(&correctAnswer, 144);
	pushBackDynamicInt64Array(&correctAnswer, 233);
	pushBackDynamicInt64Array(&correctAnswer, 377);
	pushBackDynamicInt64Array(&correctAnswer, 610);
	pushBackDynamicInt64Array(&correctAnswer, 987);
	highestNumber = 1000;
	answer = getAllFib(highestNumber);
	if(compareDynamicInt64Array(&correctAnswer, &answer) != 0){
		return false;
	}
	destroyDynamicInt64Array(&correctAnswer);
	destroyDynamicInt64Array(&answer);

	//If a false was not triggered it must have passed all tests
	return true;
}

bool testBubbleSort(){
	unsigned int NUM_TO_GENERATE = 10000;
	srand(time(0));	//Seed the random number generator
	struct DynamicInt64Array nums;
	initDynamicInt64Array(&nums);
	reserveDynamicInt64Array(&nums, NUM_TO_GENERATE);

	//Run through the appropriate number of numbers to generate and add them to the vector
	for(int cnt = 0;cnt < NUM_TO_GENERATE;++cnt){
		pushBackDynamicInt64Array(&nums, rand());
	}

	//Sort the numbers with my algorithm
	bubbleSortInt64(nums.ptr, nums.size);

	//Make sure the array is sorted
	if(!isSortedDynamicInt64Array(&nums)){
		printf("There was something wrong with the bubble sort\n");
		return false;
	}

	//If the false was not triggered then everything must have been sorted correctly
	return true;
}

bool testQuickSort(){
	unsigned int NUM_TO_GENERATE = 10000;
	srand(time(0));	//Seed the random number generator
	struct DynamicInt64Array nums;
	initDynamicInt64Array(&nums);
	reserveDynamicInt64Array(&nums, NUM_TO_GENERATE);

	//Run through the appropriate number of numbers to generate and add them to the vector
	for(int cnt = 0;cnt < NUM_TO_GENERATE;++cnt){
		pushBackDynamicInt64Array(&nums, rand());
	}

	//Sort the numbers with my algorithm
	quickSortInt64(nums.ptr, 0, nums.size - 1);

	//Make sure the array is sorted
	if(!isSortedDynamicInt64Array(&nums)){
		printf("There was something wrong with the quick sort\n");
		return false;
	}

	//If the false was not triggered then everything must have been sorted correctly
	return true;
}

/* Results:

*/
